package server

import (
	"api-gateway/service_grpc"
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"net/http"
)

type Server struct {
	service service_grpc.UserServiceGrpc
}

func New(svc *service_grpc.UserServiceGrpc) Server {
	return Server{
		service: *svc,
	}
}

func (s Server) PostRegister(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	id, err := s.service.RegisterUser(context.Background(), name)
	if err != nil {
		fmt.Println("1: ", err)
		render.JSON(w, r, err)
		return
	}
	render.JSON(w, r, id)
}
