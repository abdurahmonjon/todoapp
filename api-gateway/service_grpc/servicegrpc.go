package service_grpc

import (
	"api-gateway/proto"
	"context"
)

type UserServiceGrpc struct {
	client proto.UserServiceClient
}

func NewUserServiceGrpc(client proto.UserServiceClient) *UserServiceGrpc {
	return &UserServiceGrpc{client: client}

}
func (u UserServiceGrpc) RegisterUser(ctx context.Context, name string) (string, error) {
	id, err := u.client.RegisterUser(ctx, &proto.User{
		Name: name,
	})
	if err != nil {
		return "", err
	}
	return id.GetId(), nil

}
