package main

import (
	pb "api-gateway/proto"
	"api-gateway/server"
	"api-gateway/service_grpc"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net/http"
)

const urlusergrpc = "localhost:9009"

func main() {

	conn, err := grpc.Dial(urlusergrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("%v", err)
	}
	defer conn.Close()
	client := pb.NewUserServiceClient(conn)

	s := server.New(service_grpc.NewUserServiceGrpc(client))
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Post("/user/register/{name}", s.PostRegister)
	fmt.Println("Listening at 9999")
	http.ListenAndServe(":9999", r)
}
