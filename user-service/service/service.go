package service

import (
	"context"
	"fmt"
	"user-service/entity"
	"user-service/proto"
	"user-service/repository"
)

type Service struct {
	repo repository.Repository
}

func New(repo repository.Repository) Service {
	return Service{
		repo,
	}
}

type ServiceRepo interface {
	RegisterUser(ctx context.Context, user *proto.User) (string, error)
}

func (s Service) RegisterUser(ctx context.Context, u *proto.User) (string, error) {

	user := entity.New(u.GetName())

	if err := s.repo.RegisterUser(ctx, user); err != nil {
		fmt.Println("11: ", err)
		return "", err
	}
	return user.Id, nil

}
