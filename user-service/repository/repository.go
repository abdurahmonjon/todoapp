package repository

import (
	"context"
	"database/sql"
	"user-service/entity"
)

type Repository interface {
	RegisterUser(ctx context.Context, user entity.User) error
}

type Postgres struct {
	db *sql.DB
}

func New(db *sql.DB) Postgres {
	return Postgres{
		db: db,
	}
}

func (p Postgres) RegisterUser(ctx context.Context, user entity.User) error {
	_, er := p.db.ExecContext(ctx, `insert into users values ($1, $2)`, user.Id, user.Name)
	return er
}
