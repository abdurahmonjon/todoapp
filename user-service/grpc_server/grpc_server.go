package grpc_server

import (
	"context"
	"user-service/proto"
	"user-service/service"
)

type Server struct {
	proto.UnimplementedUserServiceServer
	service service.Service
}

func New(svc service.Service) *Server {
	return &Server{
		service: svc,
	}
}
func (s *Server) RegisterUser(ctx context.Context, user *proto.User) (*proto.UserIdResponse, error) {
	id, err := s.service.RegisterUser(ctx, user)
	if err != nil {
		return &proto.UserIdResponse{}, err
	}
	return &proto.UserIdResponse{
		Id: id,
	}, nil
}
