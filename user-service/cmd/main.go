package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"user-service/config"
	"user-service/connect"
	"user-service/grpc_server"
	"user-service/proto"
	"user-service/repository"
	service2 "user-service/service"
)

func main() {
	cfg := config.Load()
	log.Println("1")
	db, err := connect.Connect(cfg)
	log.Println("2")
	if err != nil {
		log.Println("00", err)
	}
	log.Println("3")
	repo := repository.New(db)
	svc := service2.New(repo)

	log.Println("4")
	lis, er := net.Listen("tcp", fmt.Sprintf("localhost:%s", cfg.Port))
	if er != nil {
		log.Fatalf("1: %v", er)
	}

	s := grpc.NewServer()
	proto.RegisterUserServiceServer(s, grpc_server.New(svc))
	log.Println("5")
	if er = s.Serve(lis); er != nil {
		log.Fatalf("2: %v", er)
	}
	log.Println("1")


}