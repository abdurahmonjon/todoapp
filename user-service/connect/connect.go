package connect

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"user-service/config"
)

func Connect(config config.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			config.PostgresHost, config.PostgresPort, config.PostgresUser, config.PostgresPassword, config.PostgresDB))
	if err != nil {
		log.Println("error while opening")
		return nil, err
	}
	if err = db.Ping(); err != nil {
		log.Println("error while pinging")
		return nil, err
	}
	return db, nil
}
